// Mongo
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

class Db {
  constructor() {
    this.dbName = "neural-cars";
    this.collectionName = "data";
    this.uri = "mongodb://neural:6sXBB3JLBv1sReTL@webermarci-sandbox-shard-00-00-29bwx.mongodb.net:27017,webermarci-sandbox-shard-00-01-29bwx.mongodb.net:27017,webermarci-sandbox-shard-00-02-29bwx.mongodb.net:27017/" + this.dbName + "?ssl=true&replicaSet=webermarci-sandbox-shard-0&authSource=admin";

    this.db = null;
    this.database = null;
    this.collection = null;
  }

  async connect() {
    MongoClient.connect(this.uri, async (err, db) => {
      assert.equal(null, err);
      console.log("Connected to MongoDB!");

      this.db = db;
      this.database = db.db(this.dbName);
      this.collection = this.database.collection(this.collectionName);
    });
  }

  async disconnect() {
    await this.db.close();
  }
}
module.exports = Db;