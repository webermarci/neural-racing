const express = require('express');
const bodyParser = require('body-parser');
const app = new express();

const Db = require('./db.js');
const db = new Db();

let generation;

app.use(bodyParser.json({
  limit: '100mb'
}));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/load', async (req, res, next) => {
  const data = await db.collection.findOne({});
  generation = data.generation;
  res.end(JSON.stringify(data));
});

app.post('/save', async (req, res, next) => {
  //if (generation < req.body.generation) {
    await db.collection.insertOne(req.body);
    generation = req.body.generation;
    console.log("Generation " + generation + " saved!");
  //}
  res.status(200).json({
    status: "ok"
  })
});

app.use(express.static('public'));

app.listen(3001, () => {
  console.log("Server is listening on port 3001!");
  db.connect();
});