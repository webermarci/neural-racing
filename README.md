# Neural Racing
It's a simple "follow the road" neural network driving the cars, but in this case you can draw your own road for them. This time the network gets training from the client as well, so the more you play with it the better it gets.

http://webermarci.eu/neural-racing