// Rename vars
let Neat = neataptic.Neat;
let Network = neataptic.Network;
let Methods = neataptic.methods;
let Architect = neataptic.architect;

// Turn off warnings
neataptic.config.warnings = false;

// Settings
const ROAD_SIZE = 45;
const CAR_WIDTH = ROAD_SIZE / 2.7;
const CAR_HEIGHT = CAR_WIDTH * 1.5;
const ROAD_BOUND = ROAD_SIZE * 1.7;
const POINT_SCORE = 1000;
const WIN_SCORE = 1000000;
const POINT_FORESIGHT = 16;
const POINT_FORESIGHT_STEP = 2;

const PLAYER_AMOUNT = 40;
const DEFAULT_SHOWN_CAR_AMOUNT = 1;
const START_HIDDEN_SIZE = 3;
const MUTATION_RATE = 0.3;
const ELITISM_PERCENT = 0.2;
const ITERATIONS = 0;
let visualization = false;
let allCars = false;

// Global vars
let neat;
let players = [];
let loadedBrain;
let winningCount = 0;

// Starting points
let initialPoint = null;
let initialDirection = null;
let startingPoints = [];

// Images
let carImages = [];

// Construct the genetic algorithm
function initNeat() {
  let inputSize = 1 + (POINT_FORESIGHT / POINT_FORESIGHT_STEP) * 6 + 19;
  let outputSize = 9;

  neat = new Neat(
    inputSize,
    outputSize,
    null, {
      mutation: Methods.mutation.ALL,
      popsize: PLAYER_AMOUNT,
      mutationRate: MUTATION_RATE,
      elitism: Math.round(ELITISM_PERCENT * PLAYER_AMOUNT),
      network: new Architect.Random(
        inputSize,
        START_HIDDEN_SIZE,
        outputSize
      )
    }
  );

  if (loadedBrain != null) {
    neat.generation = loadedBrain.generation;
    neat.import(loadedBrain.data);
  }
}

// Start the evaluation of the current generation
function startEvaluation() {
  players = [];

  for (let genome in neat.population) {
    genome = neat.population[genome];
    new Car(genome);
  }

  for (let i = 0; i < players.length; i++) {
    let current = players[i];
    if (i < DEFAULT_SHOWN_CAR_AMOUNT) {
      current.hidden = false;
    }
    current.image = carImages[i % carImages.length];
  }
}

// End the evaluation of the current generation
function endEvaluation(restart) {
  console.log("Generation " + neat.generation + ": " + winningCount + "/" + PLAYER_AMOUNT);

  if (winningCount >= PLAYER_AMOUNT * 0.5 && neat.generation % 10 == 0) {
    console.log("Saving");
    saveBrain();
  }

  neat.sort();
  let newPopulation = [];

  // Elitism
  for (let i = 0; i < neat.elitism; i++) {
    newPopulation.push(neat.population[i]);
  }

  // Breed the next individuals
  for (let i = 0; i < neat.popsize - neat.elitism; i++) {
    newPopulation.push(neat.getOffspring());
  }

  // Replace the old population with the new population
  neat.population = newPopulation;
  neat.mutate();

  neat.generation++;
  generationText.html("Generation: " + neat.generation);

  if (restart) {
    startEvaluation();
  } else {
    players = [];
  }

  winningCount = 0;
}