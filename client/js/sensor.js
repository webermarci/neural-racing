class Sensor {
  constructor(car, x, y, points) {
    this.car = car;
    this.direction = createVector(x, y);
    this.detected = false;

    this.xStart = 0;
    this.yStart = 0;
  }

  draw() {
    strokeWeight(2);
    if (this.detected == true) {
      stroke(200, 100, 100);
    } else {
      stroke(100, 220, 100);
    }

    push();
    translate(this.car.position.x, this.car.position.y);
    let angle = this.car.direction.heading();
    rotate(angle);
    point(this.direction.x, this.direction.y);
    pop();
  }

  detect() {
    let angle = this.car.direction.heading();
    let rotatedDirection = createVector(this.direction.x, this.direction.y);
    rotatedDirection.rotate(angle);

    let absoluteX = rotatedDirection.x + this.car.position.x;
    let absoluteY = rotatedDirection.y + this.car.position.y;

    let hit = false;
    for (let p = 0; p < this.car.nextPoints.length; p++) {
      let point = this.car.nextPoints[p].center.point;
      if (point) {
        if (collidePointCircle(absoluteX, absoluteY, point.x, point.y, ROAD_SIZE * 2.2)) {
          hit = true;
        }
      }
    }

    if (!hit) {
      this.detected = true
    } else {
      this.detected = false;
    }
  }
}