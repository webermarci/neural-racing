let canvas;
let game;
let host = "http://localhost:3001";

// UI
let startButton;
let visualizationCheckbox;
let allCarsCheckbox;
let resetButton;
let newMapButton;
let generationText;
let titleText;
let bestTimeText;
let loadingText;
let drawText;

function preload() {
  carImages[0] = loadImage(host + "/img/car_green.png");
  carImages[1] = loadImage(host + "/img/car_red.png");
  carImages[2] = loadImage(host + "/img/car_purple.png");
  carImages[3] = loadImage(host + "/img/car_orange.png");
  carImages[4] = loadImage(host + "/img/car_blue.png");
  carImages[5] = loadImage(host + "/img/car_brown.png");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  createUI();
  game = new Game();

  loadBrain().then(x => {
    loadingText.hide();
    titleText.hide();
    drawText.show();
    initNeat();
  });
}

function draw() {
  background(247, 246, 245);
  fill(20);

  game.draw();
  game.update();
}

function mouseDragged() {
  if (loadedBrain && mouseIsPressed && !game.running) {
    game.createRoad(mouseX, mouseY);
    lastAddedRoad = createVector(mouseX, mouseY);

    startButton.show();
    drawText.hide();
  }
}

async function loadBrain() {
  await fetch(host + "/load")
    .then(res => res.json())
    .then(data => {
      loadedBrain = data;
      console.log("Generation " + loadedBrain.generation + " loaded");
    }).catch((error) => {
      console.log("Error: " + error);
      drawText.html("Network error!");
    });
}

async function saveBrain() {
  let brain = {
    generation: neat.generation,
    data: neat.export()
  };

  await fetch(host + "/save", {
      method: 'POST',
      body: JSON.stringify(brain),
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    }).then(res => res.json())
    .catch(error => console.error('Error:', error));
}

function resetBrain() {
  let inputSize = 1 + (POINT_FORESIGHT / POINT_FORESIGHT_STEP) * 6 + 19;
  let outputSize = 9;

  neat = new Neat(
    inputSize,
    outputSize,
    null, {
      mutation: Methods.mutation.ALL,
      popsize: PLAYER_AMOUNT,
      mutationRate: MUTATION_RATE,
      elitism: Math.round(ELITISM_PERCENT * PLAYER_AMOUNT),
      network: new Architect.Random(
        inputSize,
        START_HIDDEN_SIZE,
        outputSize
      )
    }
  );

  saveBrain();
}

function createUI() {
  startButton = createButton('Start');
  startButton.hide();
  startButton.position(15, 15);
  startButton.mousePressed(() => {
    if (game.road.points.length >= 0) {
      game.start();
      startButton.hide();
      visualizationCheckbox.show();
      allCarsCheckbox.show();
      resetButton.show();
      newMapButton.show();
      generationText.show();
      bestTimeText.show();
    }
  });

  visualizationCheckbox = createCheckbox('Visualization', false);
  visualizationCheckbox.hide();
  visualizationCheckbox.position(15, 120);
  visualizationCheckbox.changed(() => {
    if (visualizationCheckbox.checked()) {
      visualization = true;
    } else {
      visualization = false;
    }
  });

  allCarsCheckbox = createCheckbox('Show all cars', false);
  allCarsCheckbox.hide();
  allCarsCheckbox.position(15, 145);
  allCarsCheckbox.changed(() => {
    if (allCarsCheckbox.checked()) {
      allCars = true;
    } else {
      allCars = false;
    }
  });

  resetButton = createButton('Reset');
  resetButton.hide();
  resetButton.position(15, 15);
  resetButton.mousePressed(() => {
    endEvaluation(true);
  });

  newMapButton = createButton('New map');
  newMapButton.hide();
  newMapButton.position(100, 15);
  newMapButton.mousePressed(() => {
    game.newMap();
    resetButton.hide();
    generationText.hide();
    bestTimeText.hide();
    visualizationCheckbox.hide();
    allCarsCheckbox.hide();
    newMapButton.hide();
    startButton.show();
  });

  generationText = createP("Generation: 0");
  generationText.position(20, 45);
  generationText.hide();

  bestTimeText = createP("Best time:");
  bestTimeText.position(20, 70);
  bestTimeText.hide();

  titleText = createP("Neural Racing");
  titleText.position(windowWidth / 2 - 370, windowHeight / 2 - 210);
  titleText.class("title");

  loadingText = createP("Loading...");
  loadingText.position(windowWidth / 2 + 200, windowHeight / 2 - 20);
  loadingText.class("loading");

  drawText = createP("Draw your map!");
  drawText.position(windowWidth / 2 - 95, windowHeight / 2 - 75);
  drawText.class("loading");
  drawText.hide();
}