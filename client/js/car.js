class Car {
  constructor(genome) {
    this.brain = genome;
    this.reset();

    this.width = CAR_WIDTH;
    this.height = CAR_HEIGHT;
    this.image = null;

    this.hidden = allCars ? false : true;
    this.finished = false;
    this.crashed = false;
    this.firstFrame = null;

    this.nextPoints = [];

    this.sensors = [];
    // Forward
    this.sensors.push(new Sensor(this, ROAD_SIZE * 3.5, 0));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 2.5, 0));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1.5, 0));

    this.sensors.push(new Sensor(this, ROAD_SIZE * 3, ROAD_SIZE * 0.6));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1.5, ROAD_SIZE * 0.3));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 3, -ROAD_SIZE * 0.6));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1.5, -ROAD_SIZE * 0.3));

    this.sensors.push(new Sensor(this, ROAD_SIZE * 2, ROAD_SIZE * 1));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1, ROAD_SIZE * 0.5));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 2, -ROAD_SIZE * 1));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1, -ROAD_SIZE * 0.5));

    this.sensors.push(new Sensor(this, ROAD_SIZE * 1, ROAD_SIZE * 1));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 0.5, ROAD_SIZE * 0.5));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 1, -ROAD_SIZE * 1));
    this.sensors.push(new Sensor(this, ROAD_SIZE * 0.5, -ROAD_SIZE * 0.5));

    // Side
    this.sensors.push(new Sensor(this, 0, ROAD_SIZE * 0.9));
    this.sensors.push(new Sensor(this, 0, ROAD_SIZE * 0.45));
    this.sensors.push(new Sensor(this, 0, -ROAD_SIZE * 0.9));
    this.sensors.push(new Sensor(this, 0, -ROAD_SIZE * 0.45));

    players.push(this);
  }

  reset() {
    this.position = createVector(initialPoint.x, initialPoint.y);
    this.direction = createVector(initialDirection.x, initialDirection.y);
    this.pointsPassed = 0;
    this.brain.score = 0;
  }

  think() {
    let input = [];
    let distMax = ROAD_SIZE * POINT_FORESIGHT * 2;

    if (this.nextPoints.length == 0) {
      return;
    }

    // Speed
    input.push(map(this.speed, 0, 25, 0, 1));

    // Next points
    for (let i = 0; i < this.nextPoints.length; i += POINT_FORESIGHT_STEP) {
      let center = this.nextPoints[i].center;
      let left = this.nextPoints[i].left;
      let right = this.nextPoints[i].right;

      input.push(map(center.heading, 0, PI, 0, 1));
      input.push(map(center.dist, 0, distMax, 0, 1));
      input.push(map(left.heading, 0, PI, 0, 1));
      input.push(map(left.dist, 0, distMax, 0, 1));
      input.push(map(right.heading, 0, PI, 0, 1));
      input.push(map(right.dist, 0, distMax, 0, 1));
    }

    let missingInputs = neat.input - input.length - this.sensors.length;
    for (let i = 0; i < missingInputs; i++) {
      input.push(1);
    }

    // Sensors
    for (let i = 0; i < this.sensors.length; i++) {
      let sensor = this.sensors[i];
      input.push(sensor.detected ? 1 : 0);
    }

    let output = this.brain.activate(input);
    let maxIndex = output.reduce((iMax, x, i, arr) => x > arr[iMax] ? i : iMax, 0);

    switch (maxIndex) {
      case 0:
        this.turnLeft(0.5);
        break;
      case 1:
        this.turnLeft(1);
        break;
      case 2:
        this.turnLeft(1.5);
        break;
      case 3:
        this.turnRight(0.5);
        break;
      case 4:
        this.turnRight(1);
        break;
      case 5:
        this.turnRight(1.5);
        break;
      case 6:
        this.break(0.8);
        break;
      case 7:
        this.break(0.4);
        break;
      default:
        break;
    }
  }

  update() {
    if (this.nextPoints.length > 0) {
      this.speed = this.direction.magSq();

      let nextPoint = this.nextPoints[1].center;
      this.score += (1 / nextPoint.heading) * (1 / nextPoint.dist);
    }

    this.move();
    this.think();
  }

  move() {
    if (this.direction.mag() < 6) {
      this.direction.mult(1.01);
    }
    this.position.add(this.direction);
  }

  break (amount) {
    if (this.direction.mag() > 2) {
      this.direction.mult(amount);
    }
  }

  turnLeft(amount) {
    this.direction.rotate(radians(-amount));
    if (this.direction.mag() > 3) {
      this.direction.mult(0.9 - (amount * 0.15));
    }
  }

  turnRight(amount) {
    this.direction.rotate(radians(amount));
    if (this.direction.mag() > 3) {
      this.direction.mult(0.9 - (amount * 0.15));
    }
  }

  draw() {
    if (!this.hidden && !this.crashed) {
      if (visualization) {
        this.drawSensors();
        this.drawNextPoints();
      }

      push();
      translate(this.position.x, this.position.y);

      let angle = this.direction.heading();
      rotate(angle);
      this.drawBody();
      pop();
    }
  }

  drawBody() {
    image(this.image, 0 - this.height / 2, 0 - this.width / 2, this.image.width / 1.6, this.image.height / 1.6);
  }

  drawSensors() {
    strokeWeight(1);
    for (let i = 0; i < this.sensors.length; i++) {
      this.sensors[i].draw();
    }
  }

  drawNextPoints() {
    strokeWeight(2);

    for (let i = 0; i < this.nextPoints.length; i += POINT_FORESIGHT_STEP) {
      let center = this.nextPoints[i].center;
      let left = this.nextPoints[i].left;
      let right = this.nextPoints[i].right;

      stroke(40, 200, 40);
      point(center.point.x, center.point.y);

      stroke(150, 100, 40);
      point(left.point.x, left.point.y);
      point(right.point.x, right.point.y);
    }
  }
}