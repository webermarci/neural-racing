class Game {
  constructor() {
    this.road = new Road();
    this.running = false;
    this.bestTime = null;
  }

  draw() {
    this.road.draw();

    for (let i = players.length - 1; i >= 0; i--) {
      let car = players[i];
      car.draw();
    }
  }

  update() {
    for (let i = players.length - 1; i >= 0; i--) {
      let car = players[i];

      if (car.finished || car.crashed) {
        continue;
      }

      let closestPoint = this.road.points[car.pointsPassed].center;

      let inside = collideCircleCircle(car.position.x, car.position.y, car.height, closestPoint.x, closestPoint.y, ROAD_BOUND);
      if (!inside) {
        if (!car.finished) {
          car.brain.score -= WIN_SCORE;
        }
        car.crashed = true;
      }

      if (car.firstFrame == null) {
        car.firstFrame = frameCount;
      }

      for (let j = car.pointsPassed; j < this.road.points.length; j++) {
        let dist = car.position.dist(closestPoint);
        let nextDist = car.position.dist(this.road.points[j].center);

        if (nextDist < dist) {
          closestPoint = this.road.points[j].center;

          if (car.pointsPassed < j) {
            car.brain.score += j * POINT_SCORE;
            car.pointsPassed = j;

            let points = [];
            for (let k = j - 1; k < j + POINT_FORESIGHT - 1; k++) {
              points.push(this.road.points[k]);
            }

            let currentDirection = car.direction.heading();
            car.nextPoints = [];
            for (let p = 0; p < points.length; p++) {
              let point = points[p];

              if (point) {
                push();
                translate(car.position.x, car.position.y);

                let centerPointVector = createVector(point.center.x - car.position.x, point.center.y - car.position.y);
                let leftPointVector = createVector(point.left.x - car.position.x, point.left.y - car.position.y);
                let rightPointVector = createVector(point.right.x - car.position.x, point.right.y - car.position.y);

                let centerHeading = centerPointVector.angleBetween(car.direction);
                let leftHeading = leftPointVector.angleBetween(car.direction);
                let rightHeading = rightPointVector.angleBetween(car.direction);
                pop();

                car.nextPoints.push({
                  center: {
                    point: point.center,
                    dist: car.position.dist(point.center),
                    heading: centerHeading
                  },
                  left: {
                    point: point.left,
                    dist: car.position.dist(point.left),
                    heading: leftHeading
                  },
                  right: {
                    point: point.right,
                    dist: car.position.dist(point.right),
                    heading: rightHeading
                  },
                });
              }
            }

            if (this.road.points.length > j + 1) {
              for (let s = 0; s < car.sensors.length; s++) {
                car.sensors[s].detect();
              }
            }

            if (car.pointsPassed >= this.road.points.length - 1) {
              let diff = frameCount - car.firstFrame;

              if (diff > 0) {
                if (this.bestTime == null) {
                  this.bestTime = diff;
                } else if (this.bestTime > diff) {
                  this.bestTime = diff;
                }

                bestTimeText.html("Best time: " + (this.bestTime / 60).toFixed(2) + "s");
                car.brain.score += WIN_SCORE + (WIN_SCORE / (diff / 2));
              } else {
                car.brain.score += WIN_SCORE;
              }
              car.finished = true;
              car.crashed = true;
              winningCount++;
            }
          }
          break;
        }
      }
      car.update();
    }

    let active = false;
    for (let i = 0; i < players.length; i++) {
      if (!players[i].crashed) {
        active = true;
        break;
      }
    }

    if (!active && players.length > 0) {
      endEvaluation(true);
    }
  }

  createRoad(x, y) {
    if (x - ROAD_SIZE < 0 || x + ROAD_SIZE > windowWidth || y - ROAD_SIZE < 0 || y + ROAD_SIZE > windowHeight) {
      this.road.clear();
    } else {
      this.road.add(x, y);
    }
  }

  start() {
    this.running = true;
    this.road.calculateBounds();
    startEvaluation();

    if (ITERATIONS > 0) {
      console.log("Training");
    }

    for (let i = 0; i <= ITERATIONS; i++) {
      if (i == ITERATIONS) {
        endEvaluation(true);
      }
      this.update();
    }
  }

  newMap() {
    endEvaluation(false);

    game.road.clear();
    this.running = false;
    this.bestTime = null;
    initialDirection = null;
    initialPoint = null;
    startingPoints = [];
    bestTimeText.html("Best time: ");
  }
}