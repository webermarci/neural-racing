class Road {
  constructor() {
    this.clear();
  }

  clear() {
    this.points = [];
    this.circular = false;
  }

  calculateBounds() {
    this.points = Array.from(new Set(this.points));

    let x0 = this.points[0].center.x;
    let y0 = this.points[0].center.y;
    let x1 = this.points[5].center.x;
    let y1 = this.points[5].center.y;

    if (initialPoint == null) {
      initialPoint = createVector(x0, y0);
    }

    if (initialDirection == null) {
      let x = x1 - x0;
      let y = y1 - y0;
      initialDirection = createVector(x, y).normalize();
    }

    for (let i = 0; i < this.points.length; i++) {
      let currentV = this.points[i].center;
      let nextV = (i == this.points.length - 1) ? this.points[i - 1].center : this.points[i + 1].center;

      let tangent = createVector(nextV.y - currentV.y, currentV.x - nextV.x);
      tangent.normalize();
      tangent.mult(ROAD_SIZE);

      this.points[i].left = createVector(currentV.x - tangent.x, currentV.y - tangent.y);
      this.points[i].right = createVector(currentV.x + tangent.x, currentV.y + tangent.y);
    }
  }

  add(x, y) {
    this.points.push({
      center: createVector(x, y),
      left: 0,
      right: 0
    });
  }

  draw() {
    this.drawAsphalt();
  }

  drawAsphalt() {
    noStroke();
    fill(90);

    for (let i = 0; i < this.points.length; i++) {
      let current = this.points[i];
      ellipse(current.center.x, current.center.y, ROAD_SIZE * 2.4);
    }
  }
}